# 2021

## Calendar Week 44
Nov 1st - Nov 7th

#### General 
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Triage reports / patches

#### Cycles Development
* Cleanup: Remove Cycles device checks for half float. ({{GitCommit|rB5327413b3776}}).
* Fix Cycles integrator presets. ({{GitCommit|rB1c0be7da4c36}}).
* Fix typo in Cycles PMJ enum define. ({{GitCommit|rB2b3becf2be27}}).

#### General Development
* Fix {{BugReport|92722}}: Error when saving new render preset ({{GitCommit|rBb8c573c9cd7a}}).

#### Modules
* Worked on [[User:ThomasDinges/ModuleImprovements]], a proposal to re-organize module pages and improve the information available.


## Calendar Week 45
Nov 8th - Nov 14th

#### General
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Triage reports / patches
* I was a guest on Blender.Today on Monday: https://youtu.be/qRm7uHP_Kz0
* Presented my [[User:ThomasDinges/ModuleImprovements]] report to [https://devtalk.blender.org/t/improve-module-pages-roles/21281 devtalk] and collected feedback.

#### Development
Worked on Cycles rendering on AMD GPUs with HIP:
* Reported several issues that I found {{BugReport|92972}}, {{BugReport|92975}}, {{BugReport|92984}}, {{BugReport|93045}}.
* Bisected the cause of {{BugReport|92972}} and found it in a recent half/float refactor.
* Disable graphics interop for HIP devices ({{GitCommit|rBe507a789b355}})
* Fix wrong device check in HIP kernel compile. ({{GitCommit|rB040630bb9a63}})
* Cleanup CUDA / HIP comments ({{GitCommit|rB25e7365d0d69}})

General development:
* Node Editor: Display warning when using Nishita sky texture with Eevee ({{GitCommit|rB41607ced2baf}}).

#### Release Notes
* Benchmarked GPU render time on AMD GPU between 2.93 and 3.0 for the release notes: [[User:ThomasDinges/AMDBenchmarks|AMD Benchmarks]]


## Calendar Week 46
Nov 15th - Nov 21th

#### General
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Triage reports / patches

#### Documentation
* Updated wiki to reflect new add-ons policy (commercial or linking to services outside of blender.org). [[Process/Addons]]
* Clarify that patches should be against master branch usually, after this topic has been brought up in chat. [[Tools/CodeReview#Use_a_Local_Branch]]
* Reworked the Finding Reviewers paragraph [[Process/Contributing_Code#Finding_Reviewers]]

#### Development
* Cleanup: Remove unused show_samples() device code in Cycles. ({{GitCommit|rB83a4d51997f2}}).
* Investigated NanoVDB not rendering on HIP {{BugReport|93045}}
* Checked on older Cycles bug reports and closed OpenCL reports (depcrecated now) and reports that have been fixed with Cycles X. {{BugReport|76792}}, {{BugReport|76628}}, {{BugReport|76125}}, {{BugReport|75441}}, {{BugReport|75434}}, {{BugReport|75319}}, {{BugReport|73734}}, {{BugReport|73042}}, {{BugReport|71309}}, {{BugReport|71258}}, {{BugReport|68052}}, {{BugReport|67146}}, {{BugReport|50193}}, {{BugReport|90085}}, {{BugReport|85634}}, {{BugReport|80324}}, {{BugReport|92291}}, {{BugReport|82307}}, {{BugReport|90677}}, {{BugReport|91032}}, {{BugReport|81075}}, {{BugReport|73024}}, {{BugReport|80454}}, {{BugReport|80918}}, {{BugReport|90274}}, {{BugReport|80092}}, {{BugReport|65924}}


## Calendar Week 47
Nov 22nd - Nov 28th

#### General
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Posted the Open chat, a weekly audio/video chat for every (new) developer and people who like to get involved. https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578/3
* Triage reports / patches

#### Add-ons
* Spend quite some time, clarifying and communicating about new [[Process/Addons#Key_requirements_for_Add-ons]]
* Removed Sketchfab and Archipack add-ons to reflect new requirements: ({{GitCommit|rBAC7936dde9ece8}}), ({{GitCommit|rBAd7517a6f2a69}})
* Remove the online updater from MagicUV add-on: ({{GitCommit|rBA3c5d373fc4da}})


## Calendar Week 48
Nov 29th - Dec 5th

#### General
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Offered the [https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578 Open Chat] for the first time on Monday. Next week again! 
* Fixed and updated some things for the [[Reference/Release_Notes/3.0|Blender 3.0 release notes in the wiki]].
* Added a modules status indicator to my [[User:ThomasDinges/ModuleImprovements|Module Improvements proposal]] and asked for feedback on the mailing list. 

#### Tracker
* Triage reports / patches
* Added canned response for providing a debug log on Windows.

#### Development
* Fix Amaranth and FBX add-on cycles visibility. ({{GitCommit|rBA64537a0df535}})


## Calendar Week 49
Dec 6th - Dec 12th

#### General
* Moderation and helping people on platforms (devtalk, developer, chat...)
* Offered the [https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578 Open Chat] again, this time on Tuesday. I could help some people already, answering questions on how to get started with Blender development. Will keep doing these.
* Refined [[User:ThomasDinges/ModuleImprovements]], make it more clear and asked for feedback again in the chat. 
* Triage reports / patches


## Calendar Week 50
Dec 13th - Dec 19th

#### General
* Moderation and helping people on devtalk, developer, chat...
* Offered the [https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578 Open Chat] again, this time on Wednesday.
* Triage reports / patches

#### Development
* Tested the Cycles Light Groups patch and gave feedback on UI / usability. ({{Phab|D12871}})
* Cycles: Make Embree compact BVH optional ({{Phab|D13592}})
* Fix Cycles compilation with CUDA / Optix after recent Map Range additions. ({{GitCommit|rB7e8912eb9697}})
* Fix compile error on Windows. ({{GitCommit|rB18181104592f}})


## Calendar Week 51
Dec 20th - Dec 26th

#### General
* Moderation and helping people on devtalk, developer, chat...
* Triage reports / patches
* Offered the [https://devtalk.blender.org/t/developer-community-coordinator-weekly-open-chat/21578 Open Chat] again, this time on Monday.


## Calendar Week 52
Dec 27th - Jan 2nd

Time off.
