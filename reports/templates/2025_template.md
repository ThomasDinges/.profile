# 2025

<!--
Keep the order and titles unchanged, so this can be parsed automatically.
Uncomment as you create reports, so that the most recent entry is at the top.
-->

<!--

## December 22 - 28

## December 15 - 21

## December 8 - 14

## December 1 - 7

## November 24 - 30

## November 17 - 23

## November 10 - 16

## November 3 - 9

## October 27 - November 2

## October 20 - 26

## October 13 - 19

## October 6 - 12

## September 29 - October 5

## September 22 - 28

## September 15 - 21

## September 8 - 14

## September 1 - 7

## August 25 - 31

## August 18 - 24

## August 11 - 17

## August 4 - 10

## July 28 - August 3

## July 21 - 27

## July 14 - 20

## July 7 - 13

## June 30 - July 6

## June 23 - 29

## June 16 - 22

## June 9 - 15

## June 2 - 8

## May 26 - June 1

## May 19 - 25

## May 12 - 18

## May 5 - 11

## April 28 - May 4

## April 21 - 27

## April 14 - 20

## April 7 - 13

## March 31 - April 6

## March 24 - 30

## March 17 - 23

## March 10 - 16

## March 3 - 9

## February 24 - March 2

## February 17 - 23

## February 10 - 16

## February 3 - 9

## January 27 - February 2

## January 20 - 26

## January 13 - 19

## January 6 - 12

-->

## December 30 - January 5

