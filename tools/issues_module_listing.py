#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 Blender Authors
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
# This script prints the numbers of open issues per module.
# Copy into blender/tools/triage/ to use

Example usage:

    python ./issues_module_listing.py --severity High
"""

import argparse
from datetime import date
from gitea_utils import gitea_json_issues_search

base_url = "https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels="

severity_labelid = {
    "Low": "286",
    "Normal": "287",
    "High": "285",
    "Unbreak Now!": "288"
}

modules = {
    "Module/Animation & Rigging" : {"name" : "Animation & Rigging", "buglist" : list(), "labelid": "268"},
    "Module/Core" : {"name" : "Core", "buglist" : list(), "labelid": "269"},
    "Module/EEVEE & Viewport" : {"name" : "EEVEE & Viewport", "buglist" : list(), "labelid": "272"},
    "Module/Grease Pencil" : {"name" : "Grease Pencil", "buglist" : list(), "labelid": "273"},
    "Module/Modeling" : {"name" : "Modeling", "buglist" : list(), "labelid": "274"},
    "Module/Nodes & Physics" : {"name" : "Nodes & Physics", "buglist" : list(), "labelid": "275"},
    "Module/Pipeline, Assets & IO" : {"name" : "Pipeline, Assets & I/O", "buglist" : list(), "labelid": "276"},
    "Module/Platforms, Builds, Test & Devices" : {"name" : "Platforms, Builds, Test & Devices", "buglist" : list(), "labelid": "278"},
    "Module/Python API" : {"name" : "Python API", "buglist" : list(), "labelid": "279"},
    "Module/Render & Cycles" : {"name" : "Render & Cycles", "buglist" : list(), "labelid": "280"},
    "Module/Sculpt, Paint & Texture" : {"name" : "Sculpt, Paint & Texture", "buglist" : list(), "labelid": "281"},
    "Module/User Interface" : {"name" : "User Interface", "buglist" : list(), "labelid": "283"},
    "Module/VFX & Video" : {"name" : "VFX & Video", "buglist" : list(), "labelid": "284"}
}

uncategorized_reports = list()

def compile_list(severity: str) -> None:

    label = f"Priority/{severity}"
    issues_json = gitea_json_issues_search(
        type="issues",
        state="open",
        labels=label,
        verbose=True,
    )

    for issue in issues_json:
        html_url = issue["html_url"]
        number = issue["number"]

        # Check reports module assignement and fill in data
        for label_iter in issue["labels"]:
            label = label_iter["name"]
            if label not in modules:
                continue

            modules[label]["buglist"].append(f"[#{number}]({html_url})")
            break
    else:
        uncategorized_reports.append(f"[#{number}]({html_url})")

    # Calculate total
    total = 0
    for module in modules:
        total += len(modules[module]["buglist"])

    # Print statistics
    print(f"Open {severity} Priority bugs as of {date.today()}:\n")

    uncategorized_list = (', '.join(uncategorized_reports))
    for module in modules.values():
        str_list = (', '.join(module["buglist"]))
        full_url = base_url + severity_labelid[severity] + "%2c" + module["labelid"]
        if not module["buglist"] or severity != 'High':
            print(f"- [{module['name']}]({full_url}): *{str(len(module['buglist']))}*")
        else:
            print(f"- [{module['name']}]({full_url}): *{str(len(module['buglist']))}* _{str_list}_")

    print()
    print("Total: " + str(total))

    print()
    print(f"Uncategorized: {uncategorized_list}")


def main() -> None:

    parser = argparse.ArgumentParser(
        description="Print statistics on open bug reports per module",
        epilog="This script is used to help module teams")

    parser.add_argument(
        "--severity",
        dest="severity",
        default="High",
        type=str,
        required=False,
        help="Severity of reports (Low, Normal, High, Unbreak Now!")

    args = parser.parse_args()
        
    compile_list(args.severity)


if __name__ == "__main__":
    main()
