# Generate a weekly reports template for one year

import datetime
import calendar

from pathlib import Path

# Edit this variable and run the script
# =======================================
year = 2025
# =======================================

# Last sunday of the year
for day in range(31, 0, -1):
    if calendar.weekday(year, 12, day) == calendar.SUNDAY:
        last_sunday = datetime.date(year, 12, day)
        break
else:
    raise RuntimeError(f"No Sundays in December {year}?")

INTRODUCTION = f"""# {year}

<!--
Keep the order and titles unchanged, so this can be parsed automatically.
Uncomment as you create reports, so that the most recent entry is at the top.
-->

<!--

"""

last_day = last_sunday
list_of_weeks = list()

while last_day.year == year:
    first_day = last_day - datetime.timedelta(days=6)

    month_last_day = calendar.month_name[last_day.month]
    month_first_day = calendar.month_name[first_day.month]

    if month_last_day == month_first_day:
        list_of_weeks.append(f"## {month_last_day} {first_day.day} - {last_day.day}\n\n")
    else:
        list_of_weeks.append(f"## {month_first_day} {first_day.day} - {month_last_day} {last_day.day}\n\n")

    last_day = first_day - datetime.timedelta(1)

outpath = Path(f"{year}_template.md")
with outpath.open('w') as f:
    f.write(INTRODUCTION)
    for week in list_of_weeks:
        f.write(week)
