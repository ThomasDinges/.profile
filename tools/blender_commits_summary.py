#!/usr/bin/python3

import git
import datetime
import pytz

# Set the time range between notes.
timezone = pytz.timezone('Europe/Amsterdam')
time_prev_notes = datetime.datetime(2023, 12, 4, 12, 0, 0, tzinfo=timezone)
time_next_notes = datetime.datetime(2023, 12, 11, 12, 0, 0, tzinfo=timezone)

# File name to write the notes.
today = datetime.date.today()
notes_file = f'./{today}-notes.md'

# Replace with the path to the Blender repository.
repo_path = "./blender"

# *** No need to edit below this line. *** #
# ======================================== #

notes_content = []

# Stats.
nr_commits = 0
nr_commits_fix = 0
nr_commits_cleanup = 0

# Open the repository and get the list of commits in main.
repo = git.Repo(repo_path)
commits = list(repo.iter_commits("main", reverse=True))

# Each group is a "section", e.g. Cycles, UI, etc.
commit_groups = {}
for commit in commits:
    commit_time = commit.committed_datetime.astimezone(timezone)

    # If commit is in the range of established dates.
    if ((commit_time > time_prev_notes) and (commit_time < time_next_notes)):
        author = commit.author.name
        # Title of each group, whatever is before colon.
        title = commit.summary.split(":")[0]
        try:
            summary = commit.summary.split(":")[1]
        except:
            summary = commit.summary
        commit_hash = commit.hexsha[:8]
        date = commit.committed_datetime.strftime('%y-%m-%d')

        if title in commit_groups:
            commit_groups[title].append((commit_hash, summary, author, date))
        else:
            commit_groups[title] = [(commit_hash, summary, author, date)]

# Add each commit to notes_content.
for title, group in sorted(commit_groups.items()):
    # Skip merge commits.
    merge_titles = ['Merge branch ', 'Merge remote']
    if title.startswith(tuple(merge_titles)):
        continue

    # If commit starts with Cleanup, add it to the stats.
    if title.startswith('Cleanup'):
        nr_commits_cleanup = len(group)

    # If commit starts with Fix, add it to the stats.
    if title.startswith('Fix '):
        is_fix = True
        nr_commits_fix += 1
    else:
        p_title = f"\n### {title}"
        notes_content.append(p_title)

    # Build list of commits per group.
    for commit_hash, summary, author, date in group:
        p_commit = f"* {summary} ([commit](https://projects.blender.org/blender/blender/commit/{commit_hash})) - (_{author}_)"
        notes_content.append(p_commit)

        nr_commits += 1

stats_total = f"\n## Changelog\n"
stats_total += f"{nr_commits} commits in total, of which {nr_commits_fix} are fixes and {nr_commits_cleanup} cleanup.\n"

with open(notes_file, 'w', encoding='utf8') as f:
    f.write(f'\n{stats_total}\n')
    f.write('\n'.join(notes_content))

print(stats_total)
