# Thomas Dinges

## Development Coordinator at Blender
I am responsible for onboarding, release management, module coordination and GSoC. 

### Contact
* [Chat](https://blender.chat/direct/ThomasDinges)
* [Devtalk](https://devtalk.blender.org/u/thomasdinges/)

